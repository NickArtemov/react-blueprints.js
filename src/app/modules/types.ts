import { IHomeReducerShape } from './home'

export interface IRootReducerShape {
  home: IHomeReducerShape
}
