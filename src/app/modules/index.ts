import { all } from 'redux-saga/effects'

export * from './types'

import { combineReducers } from 'redux'
import * as home from './home'
import { IRootReducerShape } from './types'

export const rootReducer = combineReducers<IRootReducerShape>({
  home: home.reducer
})

export function* saga(dispatch) {
  yield all([home.saga()])
}
