import { reducerWithInitialState } from 'typescript-fsa-reducers'
import { homeAction } from './action'
import { INITIAL_STATE } from './state'
import { IHomeReducerShape } from './types'

export const reducer = reducerWithInitialState(INITIAL_STATE).case(
  homeAction.done,
  (state: IHomeReducerShape, payload) => ({
    ...state,
    ...payload.result
  })
)
