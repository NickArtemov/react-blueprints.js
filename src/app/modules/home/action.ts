import actionCreatorFactory from 'typescript-fsa'
import { PREFIXES } from '../prefixes'
import { IHomeParams, IHomeResponse } from './types'

const actionCreator = actionCreatorFactory(PREFIXES.HOME)

export const homeAction = actionCreator.async<
  IHomeParams,
  IHomeResponse,
  Error
>('GET_HOME_DATA')
