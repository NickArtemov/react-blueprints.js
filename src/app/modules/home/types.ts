export interface IHomeReducerShape {
  data: any
}

export interface IHomeParams {
  options: any
}
export interface IHomeResponse {
  data: any
}
