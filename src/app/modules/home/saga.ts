import { all, call, put, takeLatest } from 'redux-saga/effects'
import { Action } from 'typescript-fsa'
import 'whatwg-fetch'
import { homeAction } from './action'
import { IHomeParams, IHomeResponse } from './types'

function* getHomeData(action: Action<IHomeParams>) {
  const params = action.payload
  try {
    const result: IHomeResponse = yield call(({ options }) => {
      fetch('/api', options as RequestInit)
      console.info(options)
    }, params)
    yield put(homeAction.done({ params, result }))
  } catch (error) {
    yield put(homeAction.failed({ params, error }))
  }
}

export function* saga() {
  yield all([takeLatest(homeAction.started, getHomeData)])
}
