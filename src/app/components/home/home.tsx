import { DateTime } from 'luxon'
import * as React from 'react'
import { connect } from 'react-redux'
import { homeAction, IHomeReducerShape } from '../../modules/home'
import { Button } from '../common/button'
import { IDispatchProps, IHomeProps, IStateProps } from './types'

const onClick = (props: IHomeProps) => (e: React.MouseEvent) => {
  console.info('click!', props, e)
  const options = { method: 'POST' }
  props.getHomeData({ options })
}

const Home: React.SFC<IHomeProps> = props => (
  <div className={props.className}>
    Today: {DateTime.utc().toFormat(' dd LLL yyyy')}
    <br />
    <Button onClick={onClick(props)}>🎁</Button>
  </div>
)

export default connect<IStateProps, IDispatchProps>(
  (state: IHomeReducerShape) => ({
    data: state.data
  }),
  { getHomeData: homeAction.started }
)(Home)
