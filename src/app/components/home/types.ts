import { RouteComponentProps } from 'react-router'
import { ActionCreator } from 'typescript-fsa'
import { IHomeParams, IHomeReducerShape } from '../../modules/home'

export interface IOwnProps {
  className?: string
}

export interface IStateProps {
  data: IHomeReducerShape['data']
}

export interface IDispatchProps {
  getHomeData: ActionCreator<IHomeParams>
}

export interface IHomeProps
  extends IOwnProps,
    IStateProps,
    IDispatchProps,
    RouteComponentProps<any> {}
