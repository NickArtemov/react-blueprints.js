import styled from 'styled-components'

export const Button = styled.button`
  border: 1px solid #7e7ee4;
  color: #fff;
  outline: none;
  border-radius: 3px;
  cursor: pointer;
  &:focus {
    box-shadow: 0 0 0 3px #7e7ee4;
  }
`
