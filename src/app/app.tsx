import { Settings } from 'luxon'
import * as React from 'react'
import { Provider } from 'react-redux'
import configureStore from './configureStore'
import { RoutesConnected as Root } from './routes'
const store = configureStore()

Settings.defaultLocale = 'en'

export const LinkedApp = () => (
  <Provider store={store}>
    <>
      <Root />
    </>
  </Provider>
)
