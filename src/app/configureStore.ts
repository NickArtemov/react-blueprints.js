import { createStore, compose, applyMiddleware } from 'redux'
import createSagaMiddleWare from 'redux-saga'
import { rootReducer, IRootReducerShape, saga } from './modules'

import { Store } from 'redux'

// @ts-ignore
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export default function configureStore(initialState?: IRootReducerShape) {
  const sagaMiddleWare = createSagaMiddleWare()
  const store: Store<IRootReducerShape> = createStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(sagaMiddleWare))
  )
  sagaMiddleWare.run(saga, store.dispatch)
  if (module.hot) {
    module.hot.accept('./modules', () => {
      store.replaceReducer(require('./modules').reducer)
    })
  }

  return store
}
