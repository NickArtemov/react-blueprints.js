import * as React from 'react'
import { connect } from 'react-redux'
import { Route, Router, Switch } from 'react-router-dom'
import { Home } from './components/home'
import history from './history'
import { IRootReducerShape } from './modules'
import { ROUTES } from './routesConstants'

export const Routes: React.SFC = () => (
  <Router history={history}>
    <Switch>
      <Route exact={true} path={ROUTES.ROOT} component={Home} />
    </Switch>
  </Router>
)
export const RoutesConnected = connect<{}, {}>(
  (state: IRootReducerShape) => ({})
)(Routes)
