export enum Color {
  SideBar = '#081230',
  Blue = '#09a2e4',
  Link = '#11a5e5',
  Darkest = '#20273d ',
  Dark = '#202841 ',
  Shadow = '#343d55',
  Label = '#7f838f ',
  Green = '#b8e986',
  Icon = '#bcbeb4',
  Red = '#d0021b',
  Grey = '#d3dadb',
  Hr = '#d7d8d9',
  Border = '#ddd',
  Light = '#eee',
  Lightest = '#f3f7f7',
  White = '#fff',
  RedShadow = 'rgba(208,2,27,0.3)',
  LinkShadow = 'rgba(9,162,228,0.97)',
  BlueShadow = 'rgba(9,162,228,0.3)',
  ButtonShadow = 'rgba(25,85,115,0.8)',
  RowShadow = 'rgba(9,162,228,0.05)'
}

export const FONT = '"Helvetica Neue", Helvetica, Arial, sans-serif'
